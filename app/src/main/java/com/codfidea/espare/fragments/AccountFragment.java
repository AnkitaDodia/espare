package com.codfidea.espare.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.activities.HomePageActivity;
import com.codfidea.espare.activities.SignInActivity;
import com.codfidea.espare.adapters.OrderAdapter;
import com.codfidea.espare.common.SpacesItemDecoration;
import com.codfidea.espare.model.OrderData;
import com.codfidea.espare.model.Orders;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.codfidea.espare.util.LocaleManager;
import com.codfidea.espare.util.SettingsPreferences;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

import static android.app.Activity.RESULT_OK;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class AccountFragment extends Fragment {

    String TAG = "AccountFragment";

    HomePageActivity mContext;

    LinearLayout ll_signin_flow, ll_editinfo, ll_change_language, ll_signout, layout_account_main;

    TextView txt_editinfo,txt_fullname, txt_mobilenumber, txt_address1, txt_email,txt_empty_order;

    EditText edt_fullname, edt_mobilenumber, edt_address1, edt_email;

//    Button btn_logout;


    public static final int SIGNIN_REQUEST_CODE = 1;
    private boolean isAllowEditInfo = false;

    boolean isInternet;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomePageActivity) getActivity();
        setHasOptionsMenu(true);

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView(view);
        SetListener();
        mContext.overrideFonts(layout_account_main);

        ShowViews();


    }

    private void setView(View view) {

//        btn_logout = (Button) view.findViewById(R.id.btn_logout);

        ll_signin_flow = (LinearLayout) view.findViewById(R.id.ll_signin_flow);
        ll_editinfo = (LinearLayout) view.findViewById(R.id.ll_editinfo);
        layout_account_main = (LinearLayout) view.findViewById(R.id.layout_account_main);

        ll_signout = (LinearLayout) view.findViewById(R.id.ll_signout);
        ll_change_language = (LinearLayout) view.findViewById(R.id.ll_change_language);

        txt_editinfo = (TextView) view.findViewById(R.id.txt_editinfo);
        txt_fullname = (TextView) view.findViewById(R.id.txt_fullname);
        txt_mobilenumber = (TextView) view.findViewById(R.id.txt_mobilenumber);
        txt_address1 = (TextView) view.findViewById(R.id.txt_address1);
        txt_email = (TextView) view.findViewById(R.id.txt_email);
        txt_empty_order = (TextView) view.findViewById(R.id.txt_empty_order);

        edt_fullname = (EditText) view.findViewById(R.id.edt_fullname);
        edt_mobilenumber = (EditText) view.findViewById(R.id.edt_mobilenumber);
        edt_address1 = (EditText) view.findViewById(R.id.edt_address1);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
    }

    public void SetListener() {

      /*  btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        ll_signin_flow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_cuisines = new Intent(mContext, SignInActivity.class);
                startActivityForResult(intent_cuisines, SIGNIN_REQUEST_CODE);
            }
        });

        ll_editinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext.getLogin() == 1) {
                    isAllowEditInfo =! isAllowEditInfo;

                    //TO-DO Implement UpdateDetails API
                    ShowViews();
                }else{

                    Toast.makeText(mContext, R.string.login_error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        ll_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.setLogin(0);
                SettingsPreferences.clearDB(mContext);

                Intent i = new Intent(mContext, HomePageActivity.class);
                startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        ll_change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPickDialog();
            }
        });
    }

    private void ShowViews() {

        if (mContext.getLogin() == 1) {

            ll_signin_flow.setVisibility(View.GONE);
            ll_signout.setVisibility(View.VISIBLE);
            ll_editinfo.setVisibility(View.VISIBLE);

            if (isAllowEditInfo) {

                Log.e(TAG,"isAllowEditInfo 1: "+isAllowEditInfo+"    getLogin : "+mContext.getLogin());

                txt_fullname.setVisibility(View.GONE);
                txt_mobilenumber.setVisibility(View.GONE);
                txt_address1.setVisibility(View.GONE);
                txt_email.setVisibility(View.GONE);

                edt_fullname.setVisibility(View.VISIBLE);
                edt_mobilenumber.setVisibility(View.VISIBLE);
                edt_address1.setVisibility(View.VISIBLE);
                edt_email.setVisibility(View.VISIBLE);

                edt_fullname.setText(SettingsPreferences.getConsumer(mContext).getFullname());
                edt_mobilenumber.setText(SettingsPreferences.getConsumer(mContext).getMobile());
                edt_address1.setText(SettingsPreferences.getConsumer(mContext).getAddress1());
                edt_email.setText(SettingsPreferences.getConsumer(mContext).getEmail());

                txt_editinfo.setText(R.string.title_save_details);
                txt_editinfo.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            } else {
                Log.e(TAG,"isAllowEditInfo 2: "+isAllowEditInfo+"    getLogin : "+mContext.getLogin());

                txt_fullname.setVisibility(View.VISIBLE);
                txt_mobilenumber.setVisibility(View.VISIBLE);
                txt_address1.setVisibility(View.VISIBLE);
                txt_email.setVisibility(View.VISIBLE);

                edt_fullname.setVisibility(View.GONE);
                edt_mobilenumber.setVisibility(View.GONE);
                edt_address1.setVisibility(View.GONE);
                edt_email.setVisibility(View.GONE);

                txt_fullname.setText(SettingsPreferences.getConsumer(mContext).getFullname());
                txt_mobilenumber.setText(SettingsPreferences.getConsumer(mContext).getMobile());
                txt_address1.setText(SettingsPreferences.getConsumer(mContext).getAddress1());
                txt_email.setText(SettingsPreferences.getConsumer(mContext).getEmail());

                txt_editinfo.setText(R.string.title_edit_details);
                txt_editinfo.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            }
        } else {
            ll_signin_flow.setVisibility(View.VISIBLE);
            ll_signout.setVisibility(View.GONE);
            ll_editinfo.setVisibility(View.INVISIBLE);

            Log.e(TAG,"isAllowEditInfo 3: "+isAllowEditInfo+"    getLogin : "+mContext.getLogin());

            txt_fullname.setVisibility(View.VISIBLE);
            txt_mobilenumber.setVisibility(View.VISIBLE);
            txt_address1.setVisibility(View.VISIBLE);
            txt_email.setVisibility(View.VISIBLE);

            edt_fullname.setVisibility(View.GONE);
            edt_mobilenumber.setVisibility(View.GONE);
            edt_address1.setVisibility(View.GONE);
            edt_email.setVisibility(View.GONE);

            txt_fullname.setText("N/A");
            txt_mobilenumber.setText("N/A");
            txt_address1.setText("N/A");
            txt_email.setText("N/A");
        }
    }


    public void showPickDialog() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Choose an Option");

        // add a list
        String[] Options = {"English", "Arabic"};
        builder.setItems(Options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // English

                        setNewLocale(LANGUAGE_ENGLISH, false);

                        break;
                    case 1: // Arabic

                        setNewLocale(LANGUAGE_ARABIC, false);
                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean setNewLocale(String language, boolean restartProcess) {
        LocaleManager.setNewLocale(mContext, language);

        Intent i = new Intent(mContext, HomePageActivity.class);
        startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

        if (restartProcess) {
            System.exit(0);
        } else {
            Toast.makeText(mContext, "Language Changed Successfully", Toast.LENGTH_SHORT).show();
        }
        return true;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == SIGNIN_REQUEST_CODE && resultCode == RESULT_OK) {
                ShowViews();
            }
        } catch (Exception ex) {
            Log.e("SignIn", "" + ex.toString());

        }
    }
}

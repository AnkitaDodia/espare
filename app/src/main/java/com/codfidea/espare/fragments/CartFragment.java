package com.codfidea.espare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.activities.PaymentActivity;
import com.codfidea.espare.activities.SignInActivity;
import com.codfidea.espare.adapters.CartAdapter;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.common.SpacesItemDecoration;
import com.codfidea.espare.model.AddtoCart;
import com.codfidea.espare.model.Cart;
import com.codfidea.espare.model.CartData;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.codfidea.espare.util.SettingsPreferences;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class CartFragment extends Fragment
{
    BaseActivity mContext;

    RecyclerView rcv_cartproduct;

    TextView txt_total_price, txt_empty_cartproduct;

    Button btn_checkout;

    LinearLayout layout_cart_main;

    private ArrayList<CartData> mCartList = new ArrayList<>();

    CartAdapter mCartAdapter;

    String msg;

    boolean isInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (BaseActivity) getActivity();
        setHasOptionsMenu(true);

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView(view);
        SetListners();
        mContext.overrideFonts(layout_cart_main);

        if(mContext.getLogin() == 1)
        {
            if (isInternet) {
                sendGetCartRequest();
            } else {
                Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            txt_empty_cartproduct.setVisibility(View.VISIBLE);

            rcv_cartproduct.setVisibility(View.GONE);
            txt_total_price.setVisibility(View.GONE);
            btn_checkout.setVisibility(View.GONE);

            txt_empty_cartproduct.setText(R.string.login_error);

//            Intent it = new Intent(mContext, SignInActivity.class);
//            mContext.startActivity(it);
        }
    }

    private void setView(View view)
    {
        rcv_cartproduct = (RecyclerView) view.findViewById(R.id.rcv_cartproduct);
        txt_total_price = (TextView) view.findViewById(R.id.txt_total_price);
        btn_checkout = (Button) view.findViewById(R.id.btn_checkout);

        txt_empty_cartproduct = (TextView) view.findViewById(R.id.txt_empty_cartproduct);

        layout_cart_main = (LinearLayout)view.findViewById(R.id.layout_cart_main);


        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        rcv_cartproduct.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    }

    public void SetListners() {

        btn_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mContext.getLogin() == 1)
                {
                    Intent it = new Intent(mContext, PaymentActivity.class);
                    mContext.startActivity(it);
//                    Toast.makeText(mContext,"payment process in progress",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Intent it = new Intent(mContext, SignInActivity.class);
                    mContext.startActivity(it);
                }
            }
        });
    }

    private void sendGetCartRequest() {
        mCartList.clear();

        try {
            if(mContext.mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String userid = SettingsPreferences.getConsumer(mContext).getUserId();
            Log.e("userid",""+userid);
            String jsonStr = "{\"user_id\":\"" + userid + "\"}";

            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendGetCartRequest(in, new Callback<Cart>() {
                @Override
                public void success(Cart model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CART_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {

                                mCartList = model.getData();
                                txt_total_price.setText("TOTAL : "+CalculateTotalPrice());

                            } else {
                                msg = model.getMessage();
                            }

                            InitCartRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitCartRecyler() {

        if(mCartList.size() == 0)
        {
            txt_empty_cartproduct.setVisibility(View.VISIBLE);
            txt_empty_cartproduct.setText(msg);

            rcv_cartproduct.setVisibility(View.GONE);
            txt_total_price.setVisibility(View.GONE);
            btn_checkout.setVisibility(View.GONE);
        }
        else
        {
            txt_empty_cartproduct.setVisibility(View.GONE);

            rcv_cartproduct.setVisibility(View.VISIBLE);
            txt_total_price.setVisibility(View.VISIBLE);
            btn_checkout.setVisibility(View.VISIBLE);

            mCartAdapter = new CartAdapter(mCartList, mContext, CartFragment.this);
            rcv_cartproduct.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            rcv_cartproduct.setAdapter(mCartAdapter);
        }
    }

    public void AddtoCart(String partid, String qty){
        //do stuff
        String userid = SettingsPreferences.getConsumer(mContext).getUserId();
//        Toast.makeText(mContext, "Add to Cart  "+partid+"  "+qty+"   "+userid,Toast.LENGTH_SHORT).show();

        sendAddToCartRequest(userid, partid , qty);
    }

    private void sendAddToCartRequest(String user_id, String part_id, String qty) {
        try {
            if(mContext.mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);
            String jsonStr = "{\"user_id\":\"" + user_id+ "\",\"part_id\":\"" + part_id+ "\",\"qty\":\"" + qty+ "\"}";

            Log.e("sendAddToCartRequest", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendAddToCartRequest(in, new Callback<AddtoCart>() {
                @Override
                public void success(AddtoCart model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ADD_TO_CART_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();

                                sendGetCartRequest();

                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int CalculateTotalPrice(){
        int totalprice = 0;

        for(int i = 0; i<mCartList.size(); i++){
            int tempPrice = 0;
            int price = Integer.parseInt(mCartList.get(i).getPrice());
            int qty = Integer.parseInt(mCartList.get(i).getQty());

            tempPrice = qty * price;
            totalprice = totalprice + tempPrice;
        }

        return totalprice;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }
}

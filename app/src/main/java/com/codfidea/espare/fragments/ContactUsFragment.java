package com.codfidea.espare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.activities.HomePageActivity;
import com.codfidea.espare.model.Contact;
import com.codfidea.espare.model.Data;
import com.codfidea.espare.model.User;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class ContactUsFragment extends Fragment
{
    HomePageActivity mContext;

    LinearLayout ll_contact_main;

    EditText edt_fullname_contact,edt_mobilenumber_contact,edt_address1_contact,edt_email_contact,edt_query_contact;

    Button btn_signup_one;

    boolean isInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomePageActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        setHasOptionsMenu(true);

        setView(view);
        SetListeners();
        mContext.overrideFonts(ll_contact_main);
    }

    private void setView(View view)
    {
        ll_contact_main = view.findViewById(R.id.ll_contact_main);

        edt_fullname_contact = view.findViewById(R.id.edt_fullname_contact);
        edt_mobilenumber_contact = view.findViewById(R.id.edt_mobilenumber_contact);
        edt_address1_contact = view.findViewById(R.id.edt_address1_contact);
        edt_email_contact = view.findViewById(R.id.edt_email_contact);
        edt_query_contact = view.findViewById(R.id.edt_query_contact);

        btn_signup_one = view.findViewById(R.id.btn_signup_one);
    }

    public void SetListeners() {
        btn_signup_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_fullname_contact.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_name_error,Toast.LENGTH_LONG).show();
                }
                else if(edt_mobilenumber_contact.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_mobile,Toast.LENGTH_LONG).show();
                }
                else if(edt_email_contact.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_email,Toast.LENGTH_LONG).show();
                }
                else if(edt_query_contact.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_inquiry,Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        contactRequest();
                    } else {
                        Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void contactRequest()
    {
        try {
            String name = edt_fullname_contact.getText().toString();
            String mobile = edt_mobilenumber_contact.getText().toString();
            String address = edt_address1_contact.getText().toString();
            String email = edt_email_contact.getText().toString();
            String query = edt_query_contact.getText().toString();

            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"username\":\""+name+"\",\"email\":\""+email+"\",\"mobile\":\""+mobile+"\",\"message\":\""+query+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendContactRequest(in, new Callback<Contact>() {
                @Override
                public void success(Contact model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CONTACT_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }
}

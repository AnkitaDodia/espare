package com.codfidea.espare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.activities.ClassificationActivity;
import com.codfidea.espare.activities.HomePageActivity;
import com.codfidea.espare.activities.PartProductActivity;
import com.codfidea.espare.adapters.CompanySpinnerAdapter;
import com.codfidea.espare.adapters.ModelSpinnerAdapter;
import com.codfidea.espare.adapters.YearSpinnerAdapter;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.Category;
import com.codfidea.espare.model.CategoryData;
import com.codfidea.espare.model.Company;
import com.codfidea.espare.model.CompanyData;
import com.codfidea.espare.model.Model;
import com.codfidea.espare.model.ModelData;
import com.codfidea.espare.model.Year;
import com.codfidea.espare.model.YearData;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class SearchPartsFragment extends Fragment {
    HomePageActivity mContext;

    LinearLayout ll_search_vehice, ll_search_vin_number, ll_sub_search_vehice, ll_sub_search_vin_number, ll_search_part_serial_number,
            ll_sub_search_part_serial_number,ll_all_category,layout_search_main;

    Button btn_clear_serial_number, btn_search_serial_number, btn_clear_vin_number, btn_search_vin_number, btn_clear_vehice, btn_search_vehice;

    ImageView img_arrow_vehice, img_arrow_vin, img_arrow_partserial;

    boolean isUp_search_vehice = true, isUp_search_vin_number = true, isUp_search_serial_number = true;

    Spinner spinner_make, spinner_model, spinner_year, spinner_engine;

    EditText editText_vin_number,edt_search_part_serial_number;

    private ArrayList<CompanyData> AllCompanyList = new ArrayList<>();
    private ArrayList<ModelData> AllModelList = new ArrayList<>();
    private ArrayList<YearData> YearList = new ArrayList<>();

    boolean isInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_searchautoparts, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        mContext = (HomePageActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView(view);
        SetListners();
        mContext.overrideFonts(layout_search_main);

        if (isInternet) {
            sendAllCompanyRequest();
        } else {
            Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void setView(View view) {

        ll_search_vehice = (LinearLayout) view.findViewById(R.id.ll_search_vehice);
        ll_search_vin_number = (LinearLayout) view.findViewById(R.id.ll_search_vin_number);
        ll_search_part_serial_number = (LinearLayout) view.findViewById(R.id.ll_search_part_serial_number);
        ll_sub_search_vehice = (LinearLayout) view.findViewById(R.id.ll_sub_search_vehice);
        ll_sub_search_vin_number = (LinearLayout) view.findViewById(R.id.ll_sub_search_vin_number);
        ll_sub_search_part_serial_number = (LinearLayout) view.findViewById(R.id.ll_sub_search_part_serial_number);
        ll_all_category  = (LinearLayout) view.findViewById(R.id.ll_all_category);
        layout_search_main = (LinearLayout) view.findViewById(R.id.layout_search_main);

        editText_vin_number = (EditText) view.findViewById(R.id.editText_vin_number);
        edt_search_part_serial_number = (EditText) view.findViewById(R.id.edt_search_part_serial_number);

        btn_clear_serial_number = (Button) view.findViewById(R.id.btn_clear_serial_number);
        btn_search_serial_number = (Button) view.findViewById(R.id.btn_search_serial_number);
        btn_clear_vin_number = (Button) view.findViewById(R.id.btn_clear_vin_number);
        btn_search_vin_number = (Button) view.findViewById(R.id.btn_search_vin_number);
        btn_clear_vehice = (Button) view.findViewById(R.id.btn_clear_vehice);
        btn_search_vehice = (Button) view.findViewById(R.id.btn_search_vehice);

        img_arrow_vehice = (ImageView) view.findViewById(R.id.img_arrow_vehice);
        img_arrow_vin = (ImageView) view.findViewById(R.id.img_arrow_vin);
        img_arrow_partserial = (ImageView) view.findViewById(R.id.img_arrow_partserial);

        spinner_make = (Spinner) view.findViewById(R.id.spinner_make);
        spinner_model = (Spinner) view.findViewById(R.id.spinner_model);
        spinner_year = (Spinner) view.findViewById(R.id.spinner_year);
        spinner_engine = (Spinner) view.findViewById(R.id.spinner_engine);

    }

    public void SetListners() {

        ll_search_vehice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUp_search_vehice) {
                    ll_sub_search_vehice.setVisibility(View.VISIBLE);
                    ll_sub_search_vin_number.setVisibility(View.GONE);
                    ll_sub_search_part_serial_number.setVisibility(View.GONE);

                    ll_search_vehice.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                    ll_search_vin_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    ll_search_part_serial_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    img_arrow_vehice.animate().setDuration(300).rotation(180f).start();
                } else {
                    ll_sub_search_vehice.setVisibility(View.GONE);
                    ll_search_vehice.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    img_arrow_vehice.animate().setDuration(300).rotation(0f).start();
                }

                isUp_search_vehice = !isUp_search_vehice;
            }
        });

        ll_search_vin_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUp_search_vin_number) {
                    ll_sub_search_vin_number.setVisibility(View.VISIBLE);
                    ll_search_vin_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                    ll_search_part_serial_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    ll_search_vehice.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    ll_sub_search_vehice.setVisibility(View.GONE);
                    ll_sub_search_part_serial_number.setVisibility(View.GONE);

                    img_arrow_vin.animate().setDuration(300).rotation(180f).start();
                } else {
                    ll_sub_search_vin_number.setVisibility(View.GONE);
                    ll_search_vin_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    img_arrow_vin.animate().setDuration(300).rotation(0f).start();
                }

                isUp_search_vin_number = !isUp_search_vin_number;
            }
        });

        ll_search_part_serial_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUp_search_serial_number) {
                    ll_sub_search_part_serial_number.setVisibility(View.VISIBLE);

                    ll_search_part_serial_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                    ll_search_vin_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    ll_search_vehice.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));

                    ll_sub_search_vin_number.setVisibility(View.GONE);
                    ll_sub_search_vehice.setVisibility(View.GONE);
                    img_arrow_partserial.animate().setDuration(300).rotation(180f).start();
                } else {
                    ll_sub_search_part_serial_number.setVisibility(View.GONE);
                    ll_search_part_serial_number.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    img_arrow_partserial.animate().setDuration(300).rotation(0f).start();
                }

                isUp_search_serial_number = !isUp_search_serial_number;

            }
        });


        btn_clear_serial_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edt_search_part_serial_number.setText("SDFER4573DFGE45SD");//SDFER4573DFGE45SD
            }
        });

        btn_search_serial_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edt_search_part_serial_number.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_serial,Toast.LENGTH_LONG).show();
                }
                else
                {
                    BaseActivity.searchPartMode = 1;
                    BaseActivity.serialNumber = edt_search_part_serial_number.getText().toString().trim();

                    Intent it = new Intent(mContext, PartProductActivity.class);
                    mContext.startActivity(it);

                    edt_search_part_serial_number.setText("");
                }
            }
        });

        btn_clear_vin_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editText_vin_number.setText("");
            }
        });

        btn_search_vin_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText_vin_number.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_vin,Toast.LENGTH_LONG).show();
                }
                else
                {
                    BaseActivity.searchCategoryMode = 1;
                    BaseActivity.vinNumber = editText_vin_number.getText().toString();

                    Intent it = new Intent(mContext, ClassificationActivity.class);
                    mContext.startActivity(it);

                    editText_vin_number.setText("");
                }
            }
        });

        btn_clear_vehice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        btn_search_vehice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.searchCategoryMode = 2;

                Intent it = new Intent(mContext, ClassificationActivity.class);
                mContext.startActivity(it);
            }
        });

        ll_all_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.searchCategoryMode = 3;

                Intent it = new Intent(mContext, ClassificationActivity.class);
                mContext.startActivity(it);

            }
        });

        spinner_make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                mContext.com_id = AllCompanyList.get(position).getComId();
                mContext.com_name = AllCompanyList.get(position).getComNameEnglish();

                sendAllModelRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mContext.model_id = AllModelList.get(position).getModelId();
                mContext.model_name = AllModelList.get(position).getModelNameEnglish();

                sendYearRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mContext.year_id = YearList.get(position).getYearId();
                mContext.year_name = YearList.get(position).getYear();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void sendAllCompanyRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"request_id\":\""+"all"+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendAllCompanyRequest(in, new Callback<Company>() {
                @Override
                public void success(Company model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ALLCOMPANY_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                AllCompanyList = model.getData();

                                CreateCompanySppiner();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendAllModelRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"com_id\":\""+mContext.com_id+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendAllModelRequest(in, new Callback<Model>() {
                @Override
                public void success(Model model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ALLMODEL_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                AllModelList = model.getData();

                                CreateModelSppiner();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendYearRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"model_id\":\""+mContext.model_id+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendAllYearRequest(in, new Callback<Year>() {
                @Override
                public void success(Year model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ALLYEAR_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                YearList = model.getData();

                                CreateYearSppiner();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void CreateCompanySppiner(){

        CompanySpinnerAdapter mCompanySpinnerAdapter = new CompanySpinnerAdapter(mContext, AllCompanyList);

        spinner_make.setAdapter(mCompanySpinnerAdapter);
    }

    private void CreateModelSppiner(){

        ModelSpinnerAdapter mModelSpinnerAdapter = new ModelSpinnerAdapter(mContext, AllModelList);

        spinner_model.setAdapter(mModelSpinnerAdapter);
    }

    private void CreateYearSppiner(){

        YearSpinnerAdapter mYearSpinnerAdapter = new YearSpinnerAdapter(mContext, YearList);

        spinner_year.setAdapter(mYearSpinnerAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }
}

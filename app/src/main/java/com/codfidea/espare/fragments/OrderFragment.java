package com.codfidea.espare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.activities.HomePageActivity;
import com.codfidea.espare.activities.SignInActivity;
import com.codfidea.espare.adapters.CartAdapter;
import com.codfidea.espare.adapters.OrderAdapter;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.common.SpacesItemDecoration;
import com.codfidea.espare.model.AddtoCart;
import com.codfidea.espare.model.Cart;
import com.codfidea.espare.model.CartData;
import com.codfidea.espare.model.OrderData;
import com.codfidea.espare.model.Orders;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.codfidea.espare.util.SettingsPreferences;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class OrderFragment extends Fragment
{
    HomePageActivity mContext;

    boolean isInternet;
    RecyclerView rcv_order;

    ArrayList<OrderData> mOrderList = new ArrayList<>();

    TextView txt_empty_order;

    LinearLayout ll_order_main;


    String msg;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (HomePageActivity) getActivity();
        setHasOptionsMenu(true);

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView(view);
        SetListners();
        mContext.overrideFonts(ll_order_main);

//        if(mContext.getLogin() == 1)
//        {
            if (isInternet) {
                sendOrderRequest();
            } else {
                Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
            }

//        }
    }

    private void setView(View view)
    {
        rcv_order = (RecyclerView) view.findViewById(R.id.rcv_order);
        txt_empty_order = (TextView) view.findViewById(R.id.txt_empty_order);

        ll_order_main = (LinearLayout)view.findViewById(R.id.ll_order_main);
    }

    public void SetListners() {


    }

    private void sendOrderRequest() {
        mOrderList.clear();
        try {
            if(mContext.mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

//            String userid = SettingsPreferences.getConsumer(mContext).getUserId();
            String userid = "1";
            Log.e("userid",""+userid);
            String jsonStr = "{\"user_id\":\"" + userid + "\"}";

            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendOrderRequest(in, new Callback<Orders>() {
                @Override
                public void success(Orders model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ORDER_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                mOrderList = model.getData();
                            } else {
                                msg = model.getMessage();
                            }

                            InitOrderRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitOrderRecyler()
    {
        if(mOrderList.size() == 0)
        {
            rcv_order.setVisibility(View.GONE);
            txt_empty_order.setVisibility(View.VISIBLE);
            txt_empty_order.setText(msg);
        }
        else
        {
            rcv_order.setVisibility(View.VISIBLE);
            txt_empty_order.setVisibility(View.GONE);

            OrderAdapter mOrderAdapter = new OrderAdapter(mOrderList, mContext);
            rcv_order.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
            rcv_order.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            rcv_order.setAdapter(mOrderAdapter);
        }
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }
}

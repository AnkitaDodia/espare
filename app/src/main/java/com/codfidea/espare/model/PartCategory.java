package com.codfidea.espare.model;

public class PartCategory {

    private String categotytitle;
    private int categoryimage;

    public PartCategory(String mtitle, int mcategoryimage) {
        this.categotytitle = mtitle;
        this.categoryimage = mcategoryimage;
    }


    public String getCategotytitle() {
        return categotytitle;
    }

    public void setCategotytitle(String categotytitle) {
        this.categotytitle = categotytitle;
    }

    public int getCategoryimage() {
        return categoryimage;
    }

    public void setCategoryimage(int categoryimage) {
        this.categoryimage = categoryimage;
    }

}

package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyData {

    @SerializedName("com_id")
    @Expose
    private String comId;
    @SerializedName("com_name_english")
    @Expose
    private String comNameEnglish;
    @SerializedName("com_name_arabic")
    @Expose
    private String comNameArabic;

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getComNameEnglish() {
        return comNameEnglish;
    }

    public void setComNameEnglish(String comNameEnglish) {
        this.comNameEnglish = comNameEnglish;
    }

    public String getComNameArabic() {
        return comNameArabic;
    }

    public void setComNameArabic(String comNameArabic) {
        this.comNameArabic = comNameArabic;
    }
}

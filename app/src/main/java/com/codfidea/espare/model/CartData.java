package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("part_id")
    @Expose
    private String partId;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("part_name_english")
    @Expose
    private String partNameEnglish;
    @SerializedName("part_name_arabic")
    @Expose
    private String partNameArabic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("part_number")
    @Expose
    private String partNumber;
    @SerializedName("serial_number")
    @Expose
    private String serialNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPartNameEnglish() {
        return partNameEnglish;
    }

    public void setPartNameEnglish(String partNameEnglish) {
        this.partNameEnglish = partNameEnglish;
    }

    public String getPartNameArabic() {
        return partNameArabic;
    }

    public void setPartNameArabic(String partNameArabic) {
        this.partNameArabic = partNameArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

}

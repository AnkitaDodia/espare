package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryData {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("com_id")
    @Expose
    private String comId;
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("year_id")
    @Expose
    private String yearId;
    @SerializedName("cls_id")
    @Expose
    private String clsId;
    @SerializedName("cat_name_english")
    @Expose
    private String catNameEnglish;
    @SerializedName("cat_name_arabic")
    @Expose
    private String catNameArabic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vin_number")
    @Expose
    private String vinNumber;
    @SerializedName("model_name_english")
    @Expose
    private String modelNameEnglish;
    @SerializedName("model_name_arabic")
    @Expose
    private String modelNameArabic;
    @SerializedName("com_name_english")
    @Expose
    private String comNameEnglish;
    @SerializedName("com_name_arabic")
    @Expose
    private String comNameArabic;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getClsId() {
        return clsId;
    }

    public void setClsId(String clsId) {
        this.clsId = clsId;
    }

    public String getCatNameEnglish() {
        return catNameEnglish;
    }

    public void setCatNameEnglish(String catNameEnglish) {
        this.catNameEnglish = catNameEnglish;
    }

    public String getCatNameArabic() {
        return catNameArabic;
    }

    public void setCatNameArabic(String catNameArabic) {
        this.catNameArabic = catNameArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getModelNameEnglish() {
        return modelNameEnglish;
    }

    public void setModelNameEnglish(String modelNameEnglish) {
        this.modelNameEnglish = modelNameEnglish;
    }

    public String getModelNameArabic() {
        return modelNameArabic;
    }

    public void setModelNameArabic(String modelNameArabic) {
        this.modelNameArabic = modelNameArabic;
    }

    public String getComNameEnglish() {
        return comNameEnglish;
    }

    public void setComNameEnglish(String comNameEnglish) {
        this.comNameEnglish = comNameEnglish;
    }

    public String getComNameArabic() {
        return comNameArabic;
    }

    public void setComNameArabic(String comNameArabic) {
        this.comNameArabic = comNameArabic;
    }

}

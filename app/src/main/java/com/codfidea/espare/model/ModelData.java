package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelData {

    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("com_id")
    @Expose
    private String comId;
    @SerializedName("model_name_english")
    @Expose
    private String modelNameEnglish;
    @SerializedName("model_name_arabic")
    @Expose
    private String modelNameArabic;

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getModelNameEnglish() {
        return modelNameEnglish;
    }

    public void setModelNameEnglish(String modelNameEnglish) {
        this.modelNameEnglish = modelNameEnglish;
    }

    public String getModelNameArabic() {
        return modelNameArabic;
    }

    public void setModelNameArabic(String modelNameArabic) {
        this.modelNameArabic = modelNameArabic;
    }
}

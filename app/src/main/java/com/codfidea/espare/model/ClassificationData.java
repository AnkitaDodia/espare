package com.codfidea.espare.model;

/**
 * Created by My 7 on 20-Apr-18.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassificationData
{
    @SerializedName("cls_id")
    @Expose
    private String clsId;
    @SerializedName("classification_english")
    @Expose
    private String classificationEnglish;
    @SerializedName("classification_arabic")
    @Expose
    private String classificationArabic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;

    public String getClsId() {
        return clsId;
    }

    public void setClsId(String clsId) {
        this.clsId = clsId;
    }

    public String getClassificationEnglish() {
        return classificationEnglish;
    }

    public void setClassificationEnglish(String classificationEnglish) {
        this.classificationEnglish = classificationEnglish;
    }

    public String getClassificationArabic() {
        return classificationArabic;
    }

    public void setClassificationArabic(String classificationArabic) {
        this.classificationArabic = classificationArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by My 7 on 23-Apr-18.
 */

public class OrderData {
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("part_id")
    @Expose
    private String partId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("payer_email")
    @Expose
    private String payerEmail;
    @SerializedName("payer_id")
    @Expose
    private String payerId;
    @SerializedName("txn_id")
    @Expose
    private String txnId;
    @SerializedName("mc_currency")
    @Expose
    private String mcCurrency;
    @SerializedName("payment_gross")
    @Expose
    private String paymentGross;
    @SerializedName("business_email")
    @Expose
    private String businessEmail;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("payment_date")
    @Expose
    private String paymentDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("part_name_english")
    @Expose
    private String partNameEnglish;
    @SerializedName("part_name_arabic")
    @Expose
    private String partNameArabic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("part_number")
    @Expose
    private String partNumber;
    @SerializedName("serial_number")
    @Expose
    private String serialNumber;
    @SerializedName("com_name_english")
    @Expose
    private String comNameEnglish;
    @SerializedName("com_name_arabic")
    @Expose
    private String comNameArabic;
    @SerializedName("model_name_english")
    @Expose
    private String modelNameEnglish;
    @SerializedName("model_name_arabic")
    @Expose
    private String modelNameArabic;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vin_number")
    @Expose
    private String vinNumber;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getPayerEmail() {
        return payerEmail;
    }

    public void setPayerEmail(String payerEmail) {
        this.payerEmail = payerEmail;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getMcCurrency() {
        return mcCurrency;
    }

    public void setMcCurrency(String mcCurrency) {
        this.mcCurrency = mcCurrency;
    }

    public String getPaymentGross() {
        return paymentGross;
    }

    public void setPaymentGross(String paymentGross) {
        this.paymentGross = paymentGross;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPartNameEnglish() {
        return partNameEnglish;
    }

    public void setPartNameEnglish(String partNameEnglish) {
        this.partNameEnglish = partNameEnglish;
    }

    public String getPartNameArabic() {
        return partNameArabic;
    }

    public void setPartNameArabic(String partNameArabic) {
        this.partNameArabic = partNameArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getComNameEnglish() {
        return comNameEnglish;
    }

    public void setComNameEnglish(String comNameEnglish) {
        this.comNameEnglish = comNameEnglish;
    }

    public String getComNameArabic() {
        return comNameArabic;
    }

    public void setComNameArabic(String comNameArabic) {
        this.comNameArabic = comNameArabic;
    }

    public String getModelNameEnglish() {
        return modelNameEnglish;
    }

    public void setModelNameEnglish(String modelNameEnglish) {
        this.modelNameEnglish = modelNameEnglish;
    }

    public String getModelNameArabic() {
        return modelNameArabic;
    }

    public void setModelNameArabic(String modelNameArabic) {
        this.modelNameArabic = modelNameArabic;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }
}

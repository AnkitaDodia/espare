package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PartProductData {

    @SerializedName("part_id")
    @Expose
    private String partId;
    @SerializedName("com_id")
    @Expose
    private String comId;
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("year_id")
    @Expose
    private String yearId;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("part_name_english")
    @Expose
    private String partNameEnglish;
    @SerializedName("part_name_arabic")
    @Expose
    private String partNameArabic;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("part_number")
    @Expose
    private String partNumber;
    @SerializedName("serial_number")
    @Expose
    private String serialNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("com_name_english")
    @Expose
    private String comNameEnglish;
    @SerializedName("com_name_arabic")
    @Expose
    private String comNameArabic;
    @SerializedName("model_name_english")
    @Expose
    private String modelNameEnglish;
    @SerializedName("model_name_arabic")
    @Expose
    private String modelNameArabic;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vin_number")
    @Expose
    private String vinNumber;

    public String getPartId() {
        return partId;
    }

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getPartNameEnglish() {
        return partNameEnglish;
    }

    public void setPartNameEnglish(String partNameEnglish) {
        this.partNameEnglish = partNameEnglish;
    }

    public String getPartNameArabic() {
        return partNameArabic;
    }

    public void setPartNameArabic(String partNameArabic) {
        this.partNameArabic = partNameArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComNameEnglish() {
        return comNameEnglish;
    }

    public void setComNameEnglish(String comNameEnglish) {
        this.comNameEnglish = comNameEnglish;
    }

    public String getComNameArabic() {
        return comNameArabic;
    }

    public void setComNameArabic(String comNameArabic) {
        this.comNameArabic = comNameArabic;
    }

    public String getModelNameEnglish() {
        return modelNameEnglish;
    }

    public void setModelNameEnglish(String modelNameEnglish) {
        this.modelNameEnglish = modelNameEnglish;
    }

    public String getModelNameArabic() {
        return modelNameArabic;
    }

    public void setModelNameArabic(String modelNameArabic) {
        this.modelNameArabic = modelNameArabic;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

}

package com.codfidea.espare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearData {

    @SerializedName("year_id")
    @Expose
    private String yearId;
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("vin_number")
    @Expose
    private String vinNumber;

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }
}

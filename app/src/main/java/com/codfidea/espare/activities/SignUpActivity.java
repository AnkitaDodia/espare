package com.codfidea.espare.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.Register;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class SignUpActivity extends BaseActivity {

    Context mContext;

    Button btn_signup;

    LinearLayout ll_signup_close,sign_up_main;

    EditText edt_username, edt_password, edt_fullname, edt_mobilenumber, edt_city, edt_rigion, edt_address1,
            edt_address2, edt_email;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setListener();
        overrideFonts(sign_up_main);
    }

    public void setView(){

        btn_signup = findViewById(R.id.btn_signup);

        ll_signup_close = findViewById(R.id.ll_signup_close);
        sign_up_main = findViewById(R.id.sign_up_main);

        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);
        edt_fullname = findViewById(R.id.edt_fullname);
        edt_mobilenumber = findViewById(R.id.edt_mobilenumber);
        edt_city = findViewById(R.id.edt_city);
        edt_rigion = findViewById(R.id.edt_rigion);
        edt_address1 = findViewById(R.id.edt_address1);
        edt_address2 = findViewById(R.id.edt_address2);
        edt_email = findViewById(R.id.edt_email);
    }

    private void setListener()
    {

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = edt_password.getText().toString();

                if (edt_username.getText().toString().length() == 0) {
                    edt_username.setError(getResources().getString(R.string.valid_user));
                }

                else if (edt_fullname.getText().toString().length() == 0) {
                    edt_fullname.setError(getResources().getString(R.string.valid_name));
                }

                else if (edt_mobilenumber.getText().toString().length() == 0) {
                    edt_mobilenumber.setError(getResources().getString(R.string.valid_mobile));
                }

                else if (edt_city.getText().toString().length() == 0) {
                    edt_city.setError(getResources().getString(R.string.valid_city));
                }

                else if (edt_rigion.getText().toString().length() == 0) {
                    edt_rigion.setError(getResources().getString(R.string.valid_region));
                }

                else if (edt_address1.getText().toString().length() == 0) {
                    edt_address1.setError(getResources().getString(R.string.valid_address));
                }

                else if (edt_address2.getText().toString().length() == 0) {
                    edt_address2.setError(getResources().getString(R.string.valid_address));
                }

                else if (edt_email.getText().toString().length() == 0) {
                    edt_email.setError(getResources().getString(R.string.valid_email));
                }

                else if (password.length() == 0) {
                    edt_password.setError(getResources().getString(R.string.valid_password));
                } else if (password.length() < 6) {
                    edt_password.setError(getResources().getString(R.string.password_error));
                }
                else {
                    if (isInternet) {
                        sendRegistrationRequest();
                    } else {
                        Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ll_signup_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void sendRegistrationRequest()
    {
        try {

            String username = edt_username.getText().toString();
            String password = edt_password.getText().toString();
            String fullname = edt_fullname.getText().toString();
            String mobile = edt_mobilenumber.getText().toString();
            String city = edt_city.getText().toString();
            String region = edt_rigion.getText().toString();
            String address_1 = edt_address1.getText().toString();
            String address_2 = edt_address2.getText().toString();
            String email = edt_email.getText().toString();
            String status = "1";

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"username\":\""+username+"\",\"fullname\":\""+fullname+"\",\"email\":\""+email+"\",\"mobile\":\""+mobile+"\",\"password\":\""+password+"\",\"city\":\""+city+"\"" +
                    ",\"region\":\""+region+"\",\"address_1\":\""+address_1+"\",\"address_2\":\""+address_2+"\",\"status\":\""+status+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendRegistrationRequest(in, new Callback<Register>() {
                @Override
                public void success(Register model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("REGISTER_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

package com.codfidea.espare.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.ForgotPassword;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by My 7 on 18-Apr-18.
 */

public class ForgotPasswordActivity extends BaseActivity
{
    ForgotPasswordActivity mContext;

    boolean isInternet;

    LinearLayout layout_forgot_main,ll_forget_close;

    EditText edt_forgot_username;

    Button btn_forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setClickListener();
        overrideFonts(layout_forgot_main);
    }

    private void setView()
    {
        layout_forgot_main = findViewById(R.id.layout_forgot_main);
        ll_forget_close = findViewById(R.id.ll_forget_close);

        edt_forgot_username = findViewById(R.id.edt_forgot_username);

        btn_forgot = findViewById(R.id.btn_forgot);
    }

    private void setClickListener()
    {
        ll_forget_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_forgot_username.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,R.string.valid_email_or_user, Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendForgotPasswordRequest()
    {
        try {
            String email = edt_forgot_username.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"email\":\""+email+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendForgotPasswordRequest(in, new Callback<ForgotPassword>() {
                @Override
                public void success(ForgotPassword model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("FORGOT_PASS_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();

                                finish();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

package com.codfidea.espare.activities;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.ClientToken;
import com.codfidea.espare.model.ForgotPassword;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class PaymentActivity extends BaseActivity implements PaymentMethodNonceCreatedListener {

    PaymentActivity mContext;

    boolean isInternet;

    EditText edt_fullname, edt_mobilenumber, edt_city, edt_rigion, edt_address1, edt_address2, edt_po_box, edt_postal_code,
            edt_notes;

    Button btn_payment;

    LinearLayout sign_up_main;

    BraintreeFragment mBraintreeFragment;
    private String mAuthorization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListners();
        overrideFonts(sign_up_main);

        if (isInternet) {
            sendClientTokenRequest();
        } else {
            Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        btn_payment = (Button) findViewById(R.id.btn_payment);

        sign_up_main = (LinearLayout) findViewById(R.id.sign_up_main);

        edt_fullname = (EditText) findViewById(R.id.edt_fullname);
        edt_mobilenumber = (EditText) findViewById(R.id.edt_mobilenumber);
        edt_city = (EditText) findViewById(R.id.edt_city);
        edt_rigion = (EditText) findViewById(R.id.edt_rigion);
        edt_address1 = (EditText) findViewById(R.id.edt_address1);
        edt_address2 = (EditText) findViewById(R.id.edt_address2);
        edt_po_box = (EditText) findViewById(R.id.edt_po_box);
        edt_postal_code = (EditText) findViewById(R.id.edt_postal_code);
        edt_notes = (EditText) findViewById(R.id.edt_notes);
    }

    public void SetListners() {
        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setupBraintreeAndStartExpressCheckout();
            }
        });
    }

    private void sendClientTokenRequest()
    {
        try {

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

//            String jsonStr = "{\"email\":\""+email+"\"}";
//            Log.e("jsonStr",""+jsonStr);

//            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendClientTokenRequest(new Callback<ClientToken>() {
                @Override
                public void success(ClientToken model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CLIENT_TOKEN_RESPONSE", "" + new Gson().toJson(model));

                            mAuthorization = model.getClientToken();
                            PrepareBraintree();
//                            Toast.makeText(mContext,model.getClientToken(),Toast.LENGTH_LONG).show();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void PrepareBraintree(){
        try {
            mBraintreeFragment = BraintreeFragment.newInstance(this, mAuthorization);
            // mBraintreeFragment is ready to use!
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.e("catch", "catch  : "+e);
        }
    }


    public void setupBraintreeAndStartExpressCheckout() {
        PayPalRequest request = new PayPalRequest("1")
                .currencyCode("USD")
                .intent(PayPalRequest.INTENT_AUTHORIZE);
        PayPal.requestOneTimePayment(mBraintreeFragment, request);
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        // Send nonce to server
        String nonce = paymentMethodNonce.getNonce();

        Log.e("nonce","nonce:  "+nonce);

        Toast.makeText(mContext, "nonce : "+nonce, Toast.LENGTH_SHORT).show();

        if (paymentMethodNonce instanceof PayPalAccountNonce) {
            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;

            // Access additional information
            String email = payPalAccountNonce.getEmail();
            String firstName = payPalAccountNonce.getFirstName();
            String lastName = payPalAccountNonce.getLastName();
            String phone = payPalAccountNonce.getPhone();

            // See PostalAddress.java for details
            PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
            PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
        }
    }

}

package com.codfidea.espare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codfidea.espare.R;
import com.codfidea.espare.adapters.PartProductAdapter;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.common.SpacesItemDecoration;
import com.codfidea.espare.model.AddtoCart;
import com.codfidea.espare.model.PartProduct;
import com.codfidea.espare.model.PartProductData;
import com.codfidea.espare.model.SearchBySerialNumber;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.codfidea.espare.util.SettingsPreferences;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class PartProductActivity extends BaseActivity {

    PartProductActivity mContext;

    RecyclerView rcv_partproduct;

    Button btn_change;

    LinearLayout ll_partproduct_close, ll_search_details,layout_part_product_main;

    TextView txt_search_details,txt_empty_partproduct;

    private ArrayList<PartProductData> mPartProductList = new ArrayList<>();

    PartProductAdapter mPartProductAdapter;

    PartProductData mData;

    boolean isInternet;

    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_partproduct);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListners();
        overrideFonts(layout_part_product_main);

        if (isInternet) {
            switch (BaseActivity.searchPartMode) {
                case 1: //Call Search Part by Serial number api
                    sendSearchSerialNumberRequest();
                    break;
                case 2: //Call Search Part by Category api
                    sendSearchPartRequest();
                    break;
            }
        } else {
            Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
        }

        if(BaseActivity.searchCategoryMode == 2){
            ll_search_details.setVisibility(View.VISIBLE);
            txt_search_details.setText(R.string.make+" : "+BaseActivity.com_name+",  "+R.string.model+" : "+BaseActivity.model_name+",  "+R.string.year+" : "+BaseActivity.year_name);
        }else{
            ll_search_details.setVisibility(View.GONE);
        }
    }

    private void setView()
    {
        rcv_partproduct = (RecyclerView) findViewById(R.id.rcv_partproduct);
        btn_change  = (Button) findViewById(R.id.btn_change);

        ll_partproduct_close = (LinearLayout)findViewById(R.id.ll_partproduct_close);
        ll_search_details = (LinearLayout)findViewById(R.id.ll_search_details);
        layout_part_product_main = (LinearLayout) findViewById(R.id.layout_part_product_main);

        txt_search_details = (TextView) findViewById(R.id.txt_search_details);
        txt_empty_partproduct = (TextView) findViewById(R.id.txt_empty_partproduct);
    }

    public void SetListners() {

        rcv_partproduct.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_partproduct, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_partproduct_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void sendSearchPartRequest() {
        try {
            mPartProductList.clear();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"cat_id\":\"" + BaseActivity.cat_id + "\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendSearchPartRequest(in, new Callback<PartProduct>() {
                @Override
                public void success(PartProduct model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SEARCH_PART_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                mPartProductList = model.getData();
                                msg = model.getMessage();
                            } else {
                                msg = model.getMessage();
                            }

                            InitPartCategoryRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendSearchSerialNumberRequest() {
        try {
            mPartProductList.clear();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"serial_number\":\""+BaseActivity.serialNumber+"\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendSearchSerialNumberRequest(in, new Callback<SearchBySerialNumber>() {
                @Override
                public void success(SearchBySerialNumber model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("SEARCH_SERIAL_NUMBER", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {

                                mData = model.getData();

                                mPartProductList.add(mData);

                                msg = model.getMessage();
                            } else {
                                msg = model.getMessage();
//                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }

                            InitPartCategoryRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitPartCategoryRecyler() {
        if(mPartProductList.size() == 0)
        {
            rcv_partproduct.setVisibility(View.GONE);

            txt_empty_partproduct.setText(msg);
            txt_empty_partproduct.setVisibility(View.VISIBLE);
        }
        else
        {
            rcv_partproduct.setVisibility(View.VISIBLE);
            txt_empty_partproduct.setVisibility(View.GONE);

            mPartProductAdapter = new PartProductAdapter(mPartProductList, PartProductActivity.this);

            rcv_partproduct.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

            int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
            rcv_partproduct.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
            rcv_partproduct.setAdapter(mPartProductAdapter);
        }
    }

    public void AddtoCart(String partid, String qty){
        //do stuff

        if(mContext.getLogin() == 1)
        {
            String userid = SettingsPreferences.getConsumer(mContext).getUserId();
            sendAddToCartRequest(userid, partid , qty);
        }
        else
        {
            Intent it = new Intent(mContext, SignInActivity.class);
            mContext.startActivity(it);
        }
    }

    private void sendAddToCartRequest(String user_id, String part_id, String qty) {
        try {
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);
            String jsonStr = "{\"user_id\":\"" + user_id+ "\",\"part_id\":\"" + part_id+ "\",\"qty\":\"" + qty+ "\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendAddToCartRequest(in, new Callback<AddtoCart>() {
                @Override
                public void success(AddtoCart model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("ADD_TO_CART_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

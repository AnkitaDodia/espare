package com.codfidea.espare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.codfidea.espare.R;
import com.codfidea.espare.adapters.CategoryAdapter;
import com.codfidea.espare.adapters.ClassificationAdapter;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.common.SpacesItemDecoration;
import com.codfidea.espare.model.Category;
import com.codfidea.espare.model.CategoryData;
import com.codfidea.espare.model.Classification;
import com.codfidea.espare.model.ClassificationData;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class ClassificationActivity extends BaseActivity {

    ClassificationActivity mContext;

    LinearLayout ll_classification_close,layout_classification_main;

    boolean isInternet;

    private ArrayList<CategoryData> CategoryList = new ArrayList<>();
    RecyclerView rcv_category;
    CategoryAdapter mCategoryAdapter;

    RecyclerView rcv_classification;
    ArrayList<ClassificationData> mClassificationList = new ArrayList<>();
    ClassificationAdapter mClassificationAdapter;

    SpacesItemDecoration mSpacesItemDecoration;
    TextView txt_empty_category;

    List<Integer> mClassificationSelList = new ArrayList<>();

    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_classification);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListeners();
        overrideFonts(layout_classification_main);

        if (isInternet) {
            sendClassificationRequest();

            switch (BaseActivity.searchCategoryMode) {

                case 1: //Call vin number api
                    sendSearchVinNumbersRequest();
                    break;
                case 2: //Call vehicle number api
                    sendSearchVehicleRequest();
                    break;
                case 3: //Call category api
                    sendCategorybyClassificationRequest();
                    break;
            }
        } else {
            Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        ll_classification_close = (LinearLayout) findViewById(R.id.ll_classification_close);
        layout_classification_main = (LinearLayout) findViewById(R.id.layout_classification_main);

        rcv_category = (RecyclerView) findViewById(R.id.rcv_category);
        rcv_classification = (RecyclerView) findViewById(R.id.rcv_classification);

        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.spacing);
        mSpacesItemDecoration = new SpacesItemDecoration(spacingInPixels);
        rcv_category.addItemDecoration(mSpacesItemDecoration);

        txt_empty_category = (TextView) findViewById(R.id.txt_empty_category);
    }

    public void SetListeners() {

        ll_classification_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rcv_classification.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_classification, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.cls_id = mClassificationList.get(position).getClsId();

                if (isInternet) {

                    switch (BaseActivity.searchCategoryMode) {

                        case 1: //Call vin number api
                            sendSearchVinNumbersRequest();
                            break;
                        case 2: //Call vehicle number api
                            sendSearchVehicleRequest();
                            break;
                        case 3: //Call category api
                            sendCategorybyClassificationRequest();
                            break;
                    }
                } else {
                    Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                }

                PrepareClassificationSelection(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rcv_category.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_category, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                BaseActivity.searchPartMode = 2;
                BaseActivity.cat_id = CategoryList.get(position).getCatId();
                Intent it = new Intent(mContext, PartProductActivity.class);
                mContext.startActivity(it);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendClassificationRequest() {
        try {
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"request_id\":\"all\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendClassificationRequest(in, new Callback<Classification>() {
                @Override
                public void success(Classification model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CLASSIFICATION_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                mClassificationList = model.getData();
                                BaseActivity.ClassificationList = mClassificationList;

                                InitClassificationRecyler();
                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitClassificationRecyler() {
        mClassificationAdapter = new ClassificationAdapter(mContext, mClassificationList, mClassificationSelList);
        rcv_classification.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcv_classification.setItemAnimator(new DefaultItemAnimator());
        rcv_classification.setAdapter(mClassificationAdapter);

        PrepareClassificationSelection(0);
    }

    private void PrepareClassificationSelection(int position) {
        mClassificationSelList.clear();

        for (int i = 0; i <= mClassificationList.size(); i++) {
            if (i == position) {
                mClassificationSelList.add(1);
            } else {
                mClassificationSelList.add(0);
            }
        }

        mClassificationAdapter.notifyDataSetChanged();
    }

    private void sendSearchVehicleRequest() {
        try {
            CategoryList.clear();

            if(mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr =  "{\"com_id\":\"" + mContext.com_id + "\",\"model_id\":\"" + mContext.model_id + "\",\"year_id\":\"" + mContext.year_id + "\",\"cls_id\":\"" + mContext.cls_id + "\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendSearchVehicleRequest(in, new Callback<Category>() {
                @Override
                public void success(Category model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("VEHICLE_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                CategoryList = model.getData();
                            } else {
                                msg = model.getMessage();
                            }

                            InitCategoryRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendSearchVinNumbersRequest() {
        try {
            CategoryList.clear();

            if(mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr =  "{\"vin_number\":\"" + vinNumber + "\",\"cls_id\":\"" + mContext.cls_id + "\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendSearchVinNumbersRequest(in, new Callback<Category>() {
                @Override
                public void success(Category model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("VIN_NUMBER_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                CategoryList = model.getData();
                            } else {
                                msg = model.getMessage();
                            }

                            InitCategoryRecyler();

                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendCategorybyClassificationRequest() {
        try {
            CategoryList.clear();

            if(mProgressDialogVisible())
            {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            }
            else
            {
                mContext.showWaitIndicator(true);
            }

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"cls_id\":\"" + BaseActivity.cls_id + "\"}";
            Log.e("jsonStr", "" + jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendCategorybyClassificationRequest(in, new Callback<Category>() {
                @Override
                public void success(Category model, Response response) {
                    mContext.showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("CAT_BY_CLASSIFICATION", "" + new Gson().toJson(model));

                            if (model.getStatus().equalsIgnoreCase("TRUE")) {
                                CategoryList = model.getData();
                            } else {
                                msg = model.getMessage();
                            }

                            InitCategoryRecyler();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    mContext.showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitCategoryRecyler() {

        if(CategoryList.size() == 0){
            rcv_category.setVisibility(View.GONE);
            txt_empty_category.setVisibility(View.VISIBLE);
            txt_empty_category.setText(msg);
        }
        else {
            rcv_category.setVisibility(View.VISIBLE);
            txt_empty_category.setVisibility(View.GONE);

            mCategoryAdapter = new CategoryAdapter(CategoryList, mContext);
            rcv_category.setLayoutManager(new GridLayoutManager(mContext, 2));
            rcv_category.setAdapter(mCategoryAdapter);
        }
    }
}

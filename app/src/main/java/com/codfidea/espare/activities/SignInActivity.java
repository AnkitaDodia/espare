package com.codfidea.espare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.Data;
import com.codfidea.espare.model.User;
import com.codfidea.espare.restinterface.RestInterface;
import com.codfidea.espare.util.InternetStatus;
import com.google.gson.Gson;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

public class SignInActivity extends BaseActivity {

    Context mContext;

    LinearLayout ll_signin_close,sign_in_main;

    Button btn_signup, btn_signin,btn_forgetpassword;

    EditText edt_username,edt_password;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListener();
        overrideFonts(sign_in_main);
    }

    private void setView()
    {
        ll_signin_close = findViewById(R.id.ll_signin_close);
        sign_in_main = findViewById(R.id.sign_in_main);

        btn_signin = findViewById(R.id.btn_signin);
        btn_signup = findViewById(R.id.btn_signup);
        btn_forgetpassword = findViewById(R.id.btn_forgetpassword);

        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);
    }

    private void SetListener()
    {
        ll_signin_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = getIntent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(mContext, SignUpActivity.class);
                mContext.startActivity(it);
            }
        });

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edt_username.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext, R.string.valid_user, Toast.LENGTH_SHORT).show();
                }
                else if(edt_password.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext, R.string.valid_password, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, R.string.network_error, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btn_forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(mContext, ForgotPasswordActivity.class);
                mContext.startActivity(it);
            }
        });
    }

    private void sendLoginRequest()
    {
        try {
            String email = edt_username.getText().toString();
            String password = edt_password.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            String jsonStr = "{\"email\":\""+email+"\",\"password\":\""+password+"\"}";
            Log.e("jsonStr",""+jsonStr);

            TypedInput in = new TypedByteArray("application/json", jsonStr.getBytes("UTF-8"));

            restInterface.sendLoginRequest(in, new Callback<User>() {
                @Override
                public void success(User model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));

                            if(model.getStatus().equalsIgnoreCase("TRUE"))
                            {
                                setLogin(1);
                                Data data =  model.getData();
                                updateUser(data);

                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();

                                Intent intent = getIntent();
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                            else
                            {
                                Toast.makeText(mContext,model.getMessage(),Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void failure(RetrofitError error)
                {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

package com.codfidea.espare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;

public class SplashScreen extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mContext = this;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(mContext, HomePageActivity.class);
                startActivity(i);
                finish();

            }
        }, SPLASH_TIME_OUT);
    }
}

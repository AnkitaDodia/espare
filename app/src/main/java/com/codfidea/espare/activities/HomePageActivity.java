package com.codfidea.espare.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.common.BottomNavigationViewHelper;
import com.codfidea.espare.fragments.AccountFragment;
import com.codfidea.espare.fragments.CartFragment;
import com.codfidea.espare.fragments.ContactUsFragment;
import com.codfidea.espare.fragments.OrderFragment;
import com.codfidea.espare.fragments.SearchPartsFragment;


public class HomePageActivity extends BaseActivity {
    Context mContext;

    Fragment currentFragment;

    TextView txt_toolbar_title;

    LinearLayout ll_main_close,layout_home_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        mContext = this;

        InitViews();
        SetListners();
        overrideFonts(layout_home_main);

        SearchPartsFragment mSearchPartsFragment = new SearchPartsFragment();
        replaceFragment(mSearchPartsFragment,  mContext.getResources().getString(R.string.title_fragment_autoparts));
    }


    public void InitViews() {

        txt_toolbar_title = (TextView) findViewById(R.id.txt_toolbar_title);

        layout_home_main = (LinearLayout) findViewById(R.id.layout_home_main);
        ll_main_close = (LinearLayout) findViewById(R.id.ll_main_close);
    }

    public void SetListners() {

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationViewHelper.disableShiftMode(navigation);


        ll_main_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.nav_autoparts:
                    SearchPartsFragment mSearchPartsFragment = new SearchPartsFragment();
                    replaceFragment(mSearchPartsFragment, mContext.getResources().getString(R.string.title_fragment_autoparts));

                    return true;
                case R.id.nav_profile:
                    AccountFragment mAccountFragment = new AccountFragment();
                    replaceFragment(mAccountFragment, mContext.getResources().getString(R.string.title_fragment_profile));

                    return true;
                case R.id.nav_cart:
                    CartFragment mCartFragment = new CartFragment();
                    replaceFragment(mCartFragment, mContext.getResources().getString(R.string.title_fragment_cart));

                    return true;

                case R.id.nav_order:
                    OrderFragment mOrderFragment= new OrderFragment();
                    replaceFragment(mOrderFragment, mContext.getResources().getString(R.string.title_fragment_order));
                    return true;

                case R.id.nav_contact:
                    ContactUsFragment mContactUsFragment = new ContactUsFragment();
                    replaceFragment(mContactUsFragment, mContext.getResources().getString(R.string.title_fragment_contactus));
                    return true;

            }
            return false;
        }
    };

    public void replaceFragment(Fragment fragment, String title) {
        txt_toolbar_title.setText(title);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = fragment;
    }
}

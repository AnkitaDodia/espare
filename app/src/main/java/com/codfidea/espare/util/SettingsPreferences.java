package com.codfidea.espare.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.codfidea.espare.model.Data;
import com.codfidea.espare.model.User;

import java.io.Serializable;

public class SettingsPreferences implements Serializable {

	private static final String PREFS_NAME = "eSpareDB";

	private static final String DEFAULT_VAL = null;

	private static final String Key_user_id = "user_id";
	private static final String Key_user_name = "username";
	private static final String Key_user_full_name= "fullname";
	private static final String Key_user_email= "email";
	private static final String Key_user_mobile = "mobile";
	private static final String Key_user_password = "password";
	private static final String Key_user_image = "image";
	private static final String Key_user_city = "city";
	private static final String Key_user_region = "region";
	private static final String Key_user_address_1 = "address_1";
	private static final String Key_user_address_2 = "address_2";
	private static final String Key_status = "status";
	private static final String Key_forgot = "forgot";
	private static final String Key_user_session = "user_session";

	public static void clearDB(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();
		dbEditor.clear();
		dbEditor.commit();

	}

	public static void storeConsumer(Context context, Data user) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
		Editor dbEditor = prefs.edit();

		dbEditor.putString(Key_user_id, user.getUserId());
		dbEditor.putString(Key_user_name, user.getUsername());
		dbEditor.putString(Key_user_full_name, user.getFullname());
		dbEditor.putString(Key_user_email, user.getEmail());
		dbEditor.putString(Key_user_mobile, user.getMobile());
		dbEditor.putString(Key_user_password, user.getPassword());
		dbEditor.putString(Key_user_image, user.getImage());
		dbEditor.putString(Key_user_city, user.getCity());
		dbEditor.putString(Key_user_region, user.getRegion());
		dbEditor.putString(Key_user_address_1, user.getAddress1());
		dbEditor.putString(Key_user_address_2, user.getAddress2());
		dbEditor.putString(Key_status, user.getStatus());
		dbEditor.putString(Key_forgot, user.getForgot());
		dbEditor.putString(Key_user_session, user.getUserSession());

		dbEditor.commit();
	}

	public static Data getConsumer(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);

		Data user = new Data();

		user.setAddress1(prefs.getString(Key_user_address_1, DEFAULT_VAL));
		user.setAddress2(prefs.getString(Key_user_address_2, DEFAULT_VAL));
		user.setCity(prefs.getString(Key_user_city, DEFAULT_VAL));
		user.setEmail(prefs.getString(Key_user_email, DEFAULT_VAL));
		user.setForgot(prefs.getString(Key_forgot, DEFAULT_VAL));
		user.setFullname(prefs.getString(Key_user_full_name, DEFAULT_VAL));
		user.setImage(prefs.getString(Key_user_image, DEFAULT_VAL));
		user.setMobile(prefs.getString(Key_user_mobile, DEFAULT_VAL));
		user.setPassword(prefs.getString(Key_user_password, DEFAULT_VAL));
		user.setRegion(prefs.getString(Key_user_region, DEFAULT_VAL));
		user.setStatus(prefs.getString(Key_status, DEFAULT_VAL));
		user.setUserId(prefs.getString(Key_user_id, DEFAULT_VAL));
		user.setUsername(prefs.getString(Key_user_name, DEFAULT_VAL));
		user.setUserSession(prefs.getString(Key_user_session, DEFAULT_VAL));

		return user;
	}
}

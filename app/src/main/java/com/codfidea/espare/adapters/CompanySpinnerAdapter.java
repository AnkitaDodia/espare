package com.codfidea.espare.adapters;

import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import com.codfidea.espare.R;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.CompanyData;
import com.codfidea.espare.util.LocaleManager;

import java.util.ArrayList;

import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

/**
 * Created by Raavan on 19-Apr-18.
 */

public class CompanySpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final BaseActivity mContext;
    private ArrayList<CompanyData> mAllCompanyList = new ArrayList<>();

    public CompanySpinnerAdapter(BaseActivity context, ArrayList<CompanyData> AllCompanyList) {
        this.mAllCompanyList= AllCompanyList;
        mContext = context;
    }

    public int getCount()
    {
        return mAllCompanyList.size();
    }

    public Object getItem(int i)
    {
        return mAllCompanyList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(mContext);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){

            txt.setText(mAllCompanyList.get(position).getComNameEnglish());
//            Log.e("Local","English : "+LocaleManager.getLanguage(mContext));

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){

            txt.setText(mAllCompanyList.get(position).getComNameArabic());
//            Log.e("Local","Arabic :  "+LocaleManager.getLanguage(mContext));

        }
        txt.setTextColor(Color.parseColor("#000000"));
        txt.setTypeface(mContext.getTypeFace());
        return  txt;
    }

    public View getView(int position, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(mContext);
        txt.setGravity(Gravity.CENTER);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){

            txt.setText(mAllCompanyList.get(position).getComNameEnglish());
//            Log.e("Local","English : "+LocaleManager.getLanguage(mContext));

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){

            txt.setText(mAllCompanyList.get(position).getComNameArabic());
//            Log.e("Local","Arabic :  "+LocaleManager.getLanguage(mContext));

        }
        txt.setTextColor(Color.parseColor("#000000"));
        txt.setTypeface(mContext.getTypeFace());
        return  txt;
    }

}
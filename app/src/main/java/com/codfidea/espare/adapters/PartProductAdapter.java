package com.codfidea.espare.adapters;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codfidea.espare.R;
import com.codfidea.espare.activities.PartProductActivity;
import com.codfidea.espare.model.PartProductData;
import com.codfidea.espare.util.LocaleManager;

import java.util.ArrayList;

import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

public class PartProductAdapter extends RecyclerView.Adapter<PartProductAdapter.MyViewHolder> {

    private ArrayList<PartProductData> mPartProductList;

    PartProductActivity mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_partnumber, txt_part_title, txt_price;

        public ImageView img_part, img_addtocart, img_qty_up, img_qty_down;

        public TextView txt_qty_number;

        public ConstraintLayout layout_part_item;

        public MyViewHolder(View view) {
            super(view);
            txt_partnumber = (TextView) view.findViewById(R.id.txt_partnumber);
            txt_part_title = (TextView) view.findViewById(R.id.txt_part_title);
            txt_price = (TextView) view.findViewById(R.id.txt_price);

            txt_qty_number = (TextView) view.findViewById(R.id.txt_qty_number);

            img_part = (ImageView) view.findViewById(R.id.img_part);
            img_addtocart = (ImageView) view.findViewById(R.id.img_addtocart);
            img_qty_up = (ImageView) view.findViewById(R.id.img_qty_up);
            img_qty_down = (ImageView) view.findViewById(R.id.img_qty_down);

            layout_part_item = (ConstraintLayout) view.findViewById(R.id.layout_part_item);
        }
    }

    public PartProductAdapter(ArrayList<PartProductData> PartProductList, PartProductActivity ctx) {
        this.mPartProductList = PartProductList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(mContext).inflate(R.layout.product_item, parent, false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        mContext.overrideFonts(holder.layout_part_item);

        final PartProductData mPartProduct = mPartProductList.get(position);

        holder.txt_partnumber.setText(mPartProduct.getPartNumber());
        holder.txt_price.setText(mPartProduct.getPrice()+" SAR");

        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){


            holder.txt_part_title.setText(mPartProduct.getPartNameEnglish());

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){


            holder.txt_part_title.setText(mPartProduct.getPartNameArabic());

        }

        String imgUrl = "https://espare.cc/uploads/parts/"+mPartProduct.getImage();

        Log.e("imgName", "imgUrl"+mPartProduct.getImage());
        Log.e("imgUrl", "imgUrl"+imgUrl);
        Glide.with(mContext).load(imgUrl)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_part);

        holder.img_addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext.AddtoCart(mPartProduct.getPartId(), holder.txt_qty_number.getText().toString());
            }
        });

        holder.img_qty_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int qty = Integer.parseInt(holder.txt_qty_number.getText().toString());
                qty = qty + 1;

                holder.txt_qty_number.setText(String.valueOf(qty));
            }
        });

        holder.img_qty_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(holder.txt_qty_number.getText().toString());
                if(qty != 0)
                {
                    qty = qty - 1;

                    holder.txt_qty_number.setText(String.valueOf(qty));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPartProductList.size();
    }
}
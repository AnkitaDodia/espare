package com.codfidea.espare.adapters;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codfidea.espare.R;
import com.codfidea.espare.activities.HomePageActivity;
import com.codfidea.espare.activities.PartProductActivity;
import com.codfidea.espare.common.BaseActivity;
import com.codfidea.espare.model.OrderData;
import com.codfidea.espare.model.PartProductData;
import com.codfidea.espare.util.LocaleManager;

import java.util.ArrayList;

import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    ArrayList<OrderData> mOrderList = new ArrayList<>();

    HomePageActivity mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_title, txt_serial_number, txt_part_number, txt_company, txt_model, txt_year, txt_vin_number, txt_price, txt_quantity, txt_total_price, txt_status;

        public ImageView img_order_product;

        public TextView txt_qty_number;

        public ConstraintLayout layout_order_item;

        public MyViewHolder(View view) {
            super(view);

            txt_title = (TextView) view.findViewById(R.id.txt_title);
            txt_serial_number = (TextView) view.findViewById(R.id.txt_serial_number);
            txt_company = (TextView) view.findViewById(R.id.txt_company);
            txt_part_number = (TextView) view.findViewById(R.id.txt_part_number);
            txt_model = (TextView) view.findViewById(R.id.txt_model);

            txt_year = (TextView) view.findViewById(R.id.txt_year);
            txt_vin_number = (TextView) view.findViewById(R.id.txt_vin_number);
            txt_price = (TextView) view.findViewById(R.id.txt_price);
            txt_quantity = (TextView) view.findViewById(R.id.txt_quantity);
            txt_total_price = (TextView) view.findViewById(R.id.txt_total_price);
            txt_status = (TextView) view.findViewById(R.id.txt_status);

            img_order_product = (ImageView) view.findViewById(R.id.img_order_product);

            layout_order_item = (ConstraintLayout) view.findViewById(R.id.layout_order_item);
        }
    }

    public OrderAdapter(ArrayList<OrderData> OrderList, HomePageActivity ctx) {
        this.mOrderList = OrderList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(mContext).inflate(R.layout.order_item, parent, false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        return new MyViewHolder(rootView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        mContext.overrideFonts(holder.layout_order_item);

        final OrderData mOrder = mOrderList.get(position);

        if(mOrder.getSerialNumber().length() > 12)
        {
            holder.txt_serial_number.setText(mContext.getResources().getString(R.string.serial_number)+": \n"+mOrder.getSerialNumber());
        }
        else
        {
            holder.txt_serial_number.setText(mContext.getResources().getString(R.string.serial_number)+": "+mOrder.getSerialNumber());
        }


        holder.txt_part_number.setText(mContext.getResources().getString(R.string.part_number)+": "+mOrder.getPartNumber());
        holder.txt_year.setText(mContext.getResources().getString(R.string.year)+": "+mOrder.getYear());
        holder.txt_vin_number.setText(mContext.getResources().getString(R.string.vin_number)+": "+mOrder.getVinNumber());
        holder.txt_price.setText(mContext.getResources().getString(R.string.price)+": "+mOrder.getPrice());
        holder.txt_quantity.setText(mContext.getResources().getString(R.string.qty)+": "+mOrder.getQuantity());
        holder.txt_total_price.setText(mContext.getResources().getString(R.string.total)+": "+mOrder.getTotalPrice());


        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){

            holder.txt_title.setText(mContext.getResources().getString(R.string.title)+": "+mOrder.getPartNameEnglish());
            holder.txt_company.setText(mContext.getResources().getString(R.string.company)+": "+mOrder.getComNameEnglish());
            holder.txt_model.setText(mContext.getResources().getString(R.string.model)+": "+mOrder.getModelNameEnglish());

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){

            holder.txt_title.setText(mContext.getResources().getString(R.string.title)+": "+mOrder.getPartNameArabic());
            holder.txt_company.setText(mContext.getResources().getString(R.string.company)+": "+mOrder.getComNameArabic());
            holder.txt_model.setText(mContext.getResources().getString(R.string.model)+": "+mOrder.getModelNameArabic());
        }

        if(mOrder.getStatus().equals("1")){
            holder.txt_status.setText(R.string.complete);
        }else{
            holder.txt_status.setText(R.string.pending);
        }

        String imgUrl = "https://espare.cc/uploads/parts/"+mOrder.getImage();
        Glide.with(mContext).load(imgUrl)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_order_product);
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }
}
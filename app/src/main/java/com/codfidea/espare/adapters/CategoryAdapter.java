package com.codfidea.espare.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codfidea.espare.R;
import com.codfidea.espare.activities.ClassificationActivity;
import com.codfidea.espare.model.CategoryData;
import com.codfidea.espare.util.LocaleManager;

import java.util.List;

import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<CategoryData> mCategoryList;
    ClassificationActivity mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_partscategory;

        public ImageView img_partscategory;

        public LinearLayout layout_parts_category;

        public MyViewHolder(View view) {
            super(view);
            txt_partscategory = (TextView) view.findViewById(R.id.txt_partscategory);

            img_partscategory = (ImageView) view.findViewById(R.id.img_partscategory);

            layout_parts_category = (LinearLayout) view.findViewById(R.id.layout_parts_category);
        }
    }

    public CategoryAdapter(List<CategoryData> CategoryList, ClassificationActivity ctx) {
        this.mCategoryList = CategoryList;
        this.mContext = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.partscategory_item, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CategoryData mCategory = mCategoryList.get(position);

        mContext.overrideFonts(holder.layout_parts_category);

        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){

            holder.txt_partscategory.setText(mCategory.getCatNameEnglish());
//            Log.e("Local","English : "+LocaleManager.getLanguage(mContext));

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){

            holder.txt_partscategory.setText(mCategory.getCatNameArabic());
//            Log.e("Local","Arabic :  "+LocaleManager.getLanguage(mContext));

        }



        String imgUrl = "https://espare.cc/uploads/category/"+mCategory.getImage();

        Log.e("imgUrl", "imgUrl"+imgUrl);
        Glide.with(mContext).load(imgUrl)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_partscategory);
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }
}
package com.codfidea.espare.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.codfidea.espare.R;
import com.codfidea.espare.activities.ClassificationActivity;
import com.codfidea.espare.model.ClassificationData;
import com.codfidea.espare.util.LocaleManager;

import java.util.ArrayList;
import java.util.List;

import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ARABIC;
import static com.codfidea.espare.util.LocaleManager.LANGUAGE_ENGLISH;

/**
 * Created by My 7 on 20-Apr-18.
 */

public class ClassificationAdapter extends RecyclerView.Adapter<ClassificationAdapter.MyViewHolder> {

    ArrayList<ClassificationData> mCategoryTitleList = new ArrayList<>();
    List<Integer> mClassificationSelList = new ArrayList<>();
    ClassificationActivity mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_title_classification;
        public ImageView img_title_classification;
        public LinearLayout ll_title_classification;

        public MyViewHolder(View view) {
            super(view);
            txt_title_classification = (TextView) view.findViewById(R.id.txt_title_classification);

            img_title_classification = (ImageView) view.findViewById(R.id.img_title_classification);

            ll_title_classification = (LinearLayout) view.findViewById(R.id.ll_title_classification);

        }
    }

    public ClassificationAdapter(ClassificationActivity ctx, ArrayList<ClassificationData> CategoryTitleList, List<Integer> ClassificationSelList) {
        this.mCategoryTitleList = CategoryTitleList;
        this.mContext = ctx;
        this.mClassificationSelList = ClassificationSelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classification_list_item, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ClassificationData mClassificationData= mCategoryTitleList.get(position);

        mContext.overrideFonts(holder.ll_title_classification);

        if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ENGLISH)){

            holder.txt_title_classification.setText(mClassificationData.getClassificationEnglish());

        }else if(LocaleManager.getLanguage(mContext).equals(LANGUAGE_ARABIC)){

            holder.txt_title_classification.setText(mClassificationData.getClassificationArabic());

        }



        String imgUrl = "https://espare.cc/uploads/classification/"+mClassificationData.getImage();

        Glide.with(mContext).load(imgUrl)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.img_title_classification);

        if(mClassificationSelList.get(position) == 1){
            holder.ll_title_classification.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }else{
            holder.ll_title_classification.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryTitleList.size();
    }
}
package com.codfidea.espare.restinterface;


import com.codfidea.espare.model.AddtoCart;
import com.codfidea.espare.model.Cart;
import com.codfidea.espare.model.Category;
import com.codfidea.espare.model.Classification;
import com.codfidea.espare.model.ClientToken;
import com.codfidea.espare.model.Company;
import com.codfidea.espare.model.Contact;
import com.codfidea.espare.model.ForgotPassword;
import com.codfidea.espare.model.Model;
import com.codfidea.espare.model.Orders;
import com.codfidea.espare.model.PartProduct;
import com.codfidea.espare.model.Register;
import com.codfidea.espare.model.SearchBySerialNumber;
import com.codfidea.espare.model.User;
import com.codfidea.espare.model.Year;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

/**
 * Created by Adite-Ankita on 6/30/2016.
 */
public interface RestInterface
{
    public static String API_BASE_URL = "http://espare.cc";

    String LOGIN = "/api_login";
    String REGISTER = "/api_signup";
    String FORGOT_PASSWORD = "/api-forgot";
    String CONTACT_US = "/api-contact";
    String ALLCOMPANY = "/api_company";
    String ALLMODEL = "/api_model";
    String ALLYEAR = "/api_year";
    String CLASSIFICATION = "/api_classification";

    String SEARCH_VIN_NUMBER = "/api_search_by_vin_number";
    String SEARCH_VEHICLE_NUMBER = "/api_search_by_vehicle";
    String SEARCH_SERIAL_NUMBER = "/api_search_by_serial_number";

    String SEARCH_PART = "/api_parts_search_by_category";
    String ADD_TO_CART = "/api_add_to_cart";
    String GET_CART = "/api_get_cart";

    String CATEGORY_BY_CLASSIFICATION = "/api_category_search_by_classification";

    String GET_ORDERS = "/api_get_orders";

    String GET_CLIENT_TOKEN = "/api_client_token";



    //LogIn
    @POST(LOGIN)
    public void sendLoginRequest(
            @Body TypedInput typedInput,
            Callback<User> callBack);

    //Registration
    @POST(REGISTER)
    public void sendRegistrationRequest(
            @Body TypedInput typedInput,
            Callback<Register> callBack);

    //ALL Company
    @POST(ALLCOMPANY)
    public void sendAllCompanyRequest(
            @Body TypedInput typedInput,
            Callback<Company> callBack);

    //ALL Model
    @POST(ALLMODEL)
    public void sendAllModelRequest(
            @Body TypedInput typedInput,
            Callback<Model> callBack);

    //ALL Year
    @POST(ALLYEAR)
    public void sendAllYearRequest(
            @Body TypedInput typedInput,
            Callback<Year> callBack);

    //Seach by VIN Number
    @POST(SEARCH_VIN_NUMBER)
    public void sendSearchVinNumbersRequest(
            @Body TypedInput typedInput,
            Callback<Category> callBack);

    //Seach by Vehicle
    @POST(SEARCH_VEHICLE_NUMBER)
    public void sendSearchVehicleRequest(
            @Body TypedInput typedInput,
            Callback<Category> callBack);

    //Search Result By Serial Number
    @POST(SEARCH_SERIAL_NUMBER)
    public void sendSearchSerialNumberRequest(
            @Body TypedInput typedInput,
            Callback<SearchBySerialNumber> callBack);

    // Search category by classification
    @POST(CATEGORY_BY_CLASSIFICATION)
    public void sendCategorybyClassificationRequest(
            @Body TypedInput typedInput,
            Callback<Category> callBack);

    //Forgot Password
    @POST(FORGOT_PASSWORD)
    public void sendForgotPasswordRequest(
            @Body TypedInput typedInput,
            Callback<ForgotPassword> callBack);

    //Contact
    @POST(CONTACT_US)
    public void sendContactRequest(
            @Body TypedInput typedInput,
            Callback<Contact> callBack);

    //Classification
    @POST(CLASSIFICATION)
    public void sendClassificationRequest(
            @Body TypedInput typedInput,
            Callback<Classification> callBack);

    //Search parts by category
    @POST(SEARCH_PART)
    public void sendSearchPartRequest(
            @Body TypedInput typedInput,
            Callback<PartProduct> callBack);

    //Add to Cart
    @POST(ADD_TO_CART)
    public void sendAddToCartRequest(
            @Body TypedInput typedInput,
            Callback<AddtoCart> callBack);

    //GetCart
    @POST(GET_CART)
    public void sendGetCartRequest(
            @Body TypedInput typedInput,
            Callback<Cart> callBack);

    //Get Orders
    @POST(GET_ORDERS)
    public void sendOrderRequest(
            @Body TypedInput typedInput,
            Callback<Orders> callBack);

    //Get Orders
    @GET(GET_CLIENT_TOKEN)
    public void sendClientTokenRequest(
//            @Body TypedInput typedInput,
            Callback<ClientToken> callBack);
}
